This document shows how to set up your local Ubuntu environments to run Python code with Anaconda.

## Preparing Python Environment

### 1. Download Anaconda

Download Anaconda Python 3.7 and install it based on your OS (<https://www.anaconda.com/distribution/>)

### Linux

```
$ wget https://repo.anaconda.com/archive/Anaconda3-2018.12-Linux-x86_64.sh
$ bash Anaconda3-2018.12-Linux-x86_64.sh
$ export PATH=~/anaconda3/bin:$PATH
```

### 2. Create a conda environment with Tensorflow (CPU) using terminal (or Anaconda Prompt for Windows)

Create a Conda enviroment

For CPU:

```bash
conda create -n tfcpu python=3.6 tensorflow nb_conda ipykernel
```

For GPU:

```bash
conda create -n tfgpu python=3.6 tensorflow-gpu nb_conda ipykernel
```

### 3. Activate created environment

For CPU,
```bash
source activate tfcpu
```

For GPU,
```bash
source activate tfgpu
```

### 4. Check if the Tensorflow is correctly installed

```bash
(tfcpu) $ python
Python 3.6.8 |Anaconda, Inc.| (default, Dec 29 2018, 19:04:46)
[GCC 4.2.1 Compatible Clang 4.0.1 (tags/RELEASE_401/final)] on darwin
Type "help", "copyright", "credits" or "license" for more information.
>>> import tensorflow as tf
>>> tf.__version__
'1.13.1'
>>>
```

### 5. Install Python libraries on your current Python virtual environment

Note that you have to activate your virtual Python environment first to install specific Python libraries.

```bash
(tfcpu) $ conda install scipy
Solving environment: done


==> WARNING: A newer version of conda exists. <==
  current version: 4.5.12
  latest version: 4.6.14

Please update conda by running

    $ conda update -n base -c defaults conda



# All requested packages already installed.

(tfcpu) $
```