This project is showing an example Tensorflow codes that uses CNN (Convolutional Nueral Network) and Keras to classify simple tasks on large amount of image dataset. This project is using LCC (University of Kentucky CCS) HPC to run the code by using SLURM and Singularity containers. 

If you need to install Anaconda for your local system, please check this link. (https://bitbucket.org/ywsong2/singularity_lcc/src/master/EnvSetup.md)

---
# How to install Singularity 

https://www.sylabs.io/guides/3.0/user-guide/installation.html

---

# How to pull latest TensorFlow-gpu image from CCS mirror

### Step 1. Install Singularity on your local machine

### Step 2. Pull tf-gpu-1.13.1.sif image from CCS mirror site

```bash
$ singularity pull http://mirror.ccs.uky.edu/singularity/TensorFlow/tf-gpu-1.13.1.sif
```

### Step 3. Create your own recipe file with two starting lines above like below:
```bash
Bootstrap: localimage
From: tf-gpu-1.13.1.sif

%post
   . /usr/local/tensorflow/bin/activate
   pip install --upgrade opencv-python
   pip install --upgrade tqdm
   pip install --upgrade tflearn
   pip install --upgrade scikit-learn

%runscript
   if [ $# -lt 1 ]; then
       echo "Usage: ./container PYTHON_SCRIPT"
       exit 1
   fi
   . /usr/local/tensorflow/bin/activate
   python "$@"
```
### Step 4. Build your own image
#### Note that you have to run it with 'sudo'. 

```bash
sudo singularity build my_own_name.sif my_recipe.recipe
```

---
# How to create a shell script for SLURM jobs
SLURM is the job scheduler for HPC system and LCC is using it as job scheduler. You have to write up your shell script that specifies running options of your job. Script below shows all the options that you can specify in the shell script.

```
#SBATCH --time 12:00:00         # Time limit for the job
#SBATCH --job-name=ansystest    # Job name
#SBATCH --nodes=1               # Number of nodes requiresd
#SBATCH --ntasks=8              # Number of cores required
#SBATCH --partition=P4V12_SKY32M192_S # Partition/queue to run the job in.
#SBATCH -e slurm-%j.err         # Error file
#SBATCH -o slurm-%j.out         # Output file
#SBATCH -A gol_mtjo228_uksr 
#SBATCH --mail-type ALL         # Send email when job starts/ends
#SBATCH --mail-user <enter email address here>   # Where email is sent to
#SBATCH --gres=gpu:1            # Specify number of using GPUs
```

Dr. Michael Johnson's project account name for GPU condo nodes is gol_mtjo228_uksr. The CPU jobs can be run through col_mtjo228_uksr project account.

For the --partition option, you can be selected from one of the SLURM queues for Condo resources. The list of Condo resource can be found here https://docs.ccs.uky.edu/display/UPR/SLURM+Queues. The table below shows only P100 GPU related Condo resources. Please check naming conventions on the link (https://docs.ccs.uky.edu/display/UPR/SLURM+Queues).  

|Queue Name|Max Time|Rate per Resource-Hour|Allowed Accounts|Queue Priority|
|:---:|:---:|:---:|:---:|:---:|
|P4V16_HAS16M128_L|	72 hours|	1 GPU SU|	g*l	|N/A|
|P4V12_SKY32M192_L|	72 hours|	1 GPU SU|	g*l|	0|
|P4V12_SKY32M192_M|	24 hours|	1 GPU SU|	g*l	|5000|
|P4V12_SKY32M192_S|	12 hours|	1 GPU SU|	g*l	|10000|
|P4V12_SKY32M192_D|	1 hour|	0 GPU SUs|	gol|	N/A|

The shell script below shows example shell script that runs CNN sample codes to submit a job using SLURM. Note that it runs the singularity container at the end of script with other input arguments.

```
#SBATCH --time 02:00:00         # Time limit for the job
#SBATCH --job-name=Demo_GPU     # Job name
#SBATCH --nodes=1               # Number of nodes requiresd
#SBATCH --ntasks=8              # Number of cores required
#SBATCH --partition=P4V12_SKY32M192_S # Partition/queue to run the job in.
#SBATCH -e slurm-%j.err         # Error file
#SBATCH -o slurm-%j.out         # Output file
#SBATCH -A gol_mtjo228_uksr 
#SBATCH --mail-type ALL         # Send email when job starts/ends
#SBATCH --mail-user ywsong2@uky.edu   # Where email is sent to
#SBATCH --gres=gpu:1             Specify number of using GPUs

echo "Extracting research data..."
tar -C /tmp -xzf /scratch/$USER/TrainDataKeras.tar.gz
echo "Done extracting."

module load ccs/singularity
singularity run --nv tf-gpu.sif SCNN.py -g 1 -c 8 -l 0.00005 -d /tmp/TrainDataKeras/
```

You can download sample data to run the example code from here - https://drive.google.com/open?id=11UfDpUGuRFoY9fLO6mtjpkkCbr1G62IA.

---

# How to submit a job on LCC using SLURM scheduler

After you create your shell sciprt for your own job, you need to convert the shell script to executable format by using 'chmod' command like below.

```bash
[ywsong2@login001 ywsong2]$ chmod +x your_shell_script.sh
```

Once it is converted to executable shell script, you can simply submit your job using SLURM's sbatch command like below. Once it is successfully submitted, SLURM will give you the job id of your submitted job.

```bash
[ywsong2@login001 ywsong2]$ sbatch your_shell_script.sh
Submitted batch job 143113
```

---

# How to check submitted job status on LCC

After submitting a job, you can check status of your job on SLURM queue using squeue command like below. You can specify your linkblue ID after -u to get your job lists only.

```bash
[ywsong2@login001 ywsong2]$ squeue -u ywsong2
             JOBID PARTITION     NAME     USER ST       TIME  NODES NODELIST(REASON)
            141174 P4V12_SKY TF_SCNN_  ywsong2 PD       0:00      1 (Resources)
[ywsong2@login001 ywsong2]$
```

---

# How to cancel your job on LCC

To cancel your job on LCC, you can use scancel command like below. As you can see the command below, you have to specify the job id as input argument that you got from your submission.

```bash
[ywsong2@login001 ywsong2]$ scancel 141174
```

---

# How to check your output log of the running job on LCC

You can keep watching output file (.out) to monitor log messages from your job using tail command like below.

```bash
[ywsong2@login001 ywsong2]$ tail -f slurm-143932.out

-- Start training --
Epoch 1/10
420/420 [==============================] - 152s 362ms/step - loss: 5.1948 - acc: 0.6775 - val_loss: 5.3374 - val_acc: 0.6689
Epoch 2/10
420/420 [==============================] - 145s 344ms/step - loss: 5.1930 - acc: 0.6778 - val_loss: 5.1889 - val_acc: 0.6781
Epoch 3/10
420/420 [==============================] - 145s 344ms/step - loss: 5.1885 - acc: 0.6781 - val_loss: 5.1889 - val_acc: 0.6781
```
---

# Where should you to run your code and save your data in LCC? 

You should use your scratch folder on LCC to run jobs. Your data can be saved on scratch folder as well. Your scratch folder is located at /scratch/your_link_blue_id and it has 25TB quota. You can access to your scratch folder using cd command after you ssh into LCC as shown below.

```
ywsong2$ ssh ywsong2@lcc.uky.edu
ywsong2@lcc.uky.edu's password: (Enter your UK linkblue password)
Last login: Thu May 16 15:17:50 2019 from 172.31.40.235

                      LIPSCOMB COMPUTE CLUSTER

 ---------------------------------------------------------------
| Need help? Email help-hpc@uky.edu to create a support ticket. |
 ---------------------------------------------------------------

NOTICE: May 14, 2019
To load Singularity, use "module load ccs/singularity". All other Singularity
modulefiles have been removed, and this is now the only legal way to load
Singularity.

NOTICE: May 14, 2019
All jobs now require an explicit time limit. Add the "-t <TIME>" option to your
sbatch scripts or command-line arguments. Jobs will automatically be killed if
they exceed their time limits. Acceptable time formats for <TIME> include
"minutes", "minutes:seconds", "hours:minutes:seconds", "days-hours",
"days-hours:minutes" and "days-hours:minutes:seconds". The maximum allowed time
limit varies by queue (see sinfo).

NOTICE: May 14, 2019
The debug queues now only accept one job per PI. Furthermore, the GPU debug
queue only accepts jobs requesting a single GPU (--gres=gpu or --gres=gpu:1),
up to 8 cores, and up to 48 GB of memory. Jobs submitted to the GPU debug queue
which request more than these resources will pend forever.

[ywsong2@login002 ~]$ cd /scratch/ywsong2/
[ywsong2@login002 ywsong2]$
```

# Example of using debug node to run interactive bash job

```bash
srun -A gol_mtjo228_uksr -p P4V12_SKY32M192_D -t 01:00:00 --gres=gpu --pty bash
```