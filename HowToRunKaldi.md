# An Instruction to install and run Kaldi on LCC cluster:

The first thing we need to run Kaldi on LCC is a container in which kaldi has been installed. Having
such a container, we can do it in several ways, installing the Kladi by ourselves or using an existing
container. Here, I used the NVIDIA container since we can modify itand install what we need in our
project. However, based on the container property and what we need to change for installing of Kaldi,
there are some directory issues and we have to change the container and run the Kaldi outside of the
Klaid directory. To have better understanding, I will give you each step that I have done.

First,make and navigate to your favorite directory:
For example:
```
$ mkdir ~/KaldiSing (make this a folder with KaldiSing Name in your home directory)
$ cd ~/ KaldiSing
```

Now the only thing we need to do in our local machine is pulling the following .sif file from the
NVIDIA container and build a writable container as following:

```
$ sudo singularity build --sandbox KaldiDev/ docker://nvcr.io/nvidia/kaldi:19.05-py3
```

To make changes within the container, we need to change the container into the writable mode

```
$ sudo singularity shell --writable KaldiDev/
```

And then install the IRSTLM since we cannot install in root directory on LCC cluster but we can get
access to the root directory.
```
$ cd /opt/kaldi/tools
$ extras/install_irstlm.sh
```
So far, we install the kaldi with IRSTLM. Now, we convert the Sandbox container to .sif one with the
following command:
```
$ sudo singularity build MyKaldiWithNvidia.sif KaldiDev/
```
At this stage, we should send the files below to LCC cluster of the UK. Before sending the files to
LCC, you can make a directory (folder) to organize the project better.
```
$ cd /scratch/UkBlueId
$ mkdir KladiProject
```

## 1) MyKaldiWithNvidia.sif
```
scp MyKaldiWithNvidia.sif UkBlueId@lcc.uky.edu:/scratch/UkBlueId/KladiProject
```

## 2) Timit database

Before being able to send the Timit database, we need to zip it first and then send it as following:

```
scp Timit.tar.xz UkBlueId@lcc.uky.edu:/scratch/UkBlueId/KladiProject
```

#### Note: you can use different methods of compression such as zip, tar.xz and tar.gz.

Up to this point, we have whatever we need on LCC cluster to run Kaldi for Timit database. So, we
need to make the following soft links. Before that, we should go inside of the container:

```
$ modal load ccs/singularity
$ singularity shell MyKaldiWithNvidia.sif
```
and then:

```
ln -s /opt/kaldi/egs/timit/s5/steps .
ln -s /opt/kaldi/egs/timit/s5/utils .
ln -s /opt/kaldi/egs/timit/s5/local .
ln -s /opt/kaldi/egs/timit/s5/conf .
ln -s /opt/kaldi/src .
```
In addition to the directories, we also need a copy of the path.sh and cmd.sh script in our directory.
```
cp /opt/kaldi/egs/timit/s5/path.sh .
cp /opt/kaldi/egs/timit/s5/cmd.sh .
cp /opt/kaldi/egs/timit/s5/run.sh .
```
Note: you can make a bash file and put all above commands and run the bash file.
After creating the above links and files, we need to make a change in path.sh like below in order to find the KALDI_ROOT.

Comment the first line:
```
#export KALDI_ROOT=$(readlink -f $(readlink -f $PWD)/opt/kaldi)
```
and add this line there:
```
export KALDI_ROOT=/opt/kaldi
```
Now, we have the essential directory structures and all the necessary soft links to the Timit recipe
outside of Kaldi. We need to change the run.sh script to give appropriate path for the TIMIT database
as below:
```
echo ============================================================================
echo " Data & Lexicon & Language Preparation "
echo ============================================================================
#timit=/export/corpora5/LDC/LDC93S1/timit/TIMIT # @JHU
timit=/export/corpora5/LDC/LDC93S1/timit/TIMIT # @BUT
```
Change the red line to the following green line:
```
timit=/scratch/UkBlueId/KaldiProject/Timit # @BUT
```
Since the project should be run on LCC cluster, the slurm should be selected in cmd.sh file:
To edit the file:
```
$ vim cmd.sh
```
And change the following variables:
```
train_cmd="slurm.pl"
train_cmd="slurm.pl"
train_cmd="slurm.pl"
```
Another step is scripting a slrum job:
Make a file with vim:
```
vim KaldiJobSub.sh
```
And put the green script there and save it.
```
#!/bin/bash
#SBATCH --time 01:00:00 # Time limit for the job
#SBATCH --job-name=Kladi_GPU # Job name
#SBATCH --nodes=1 # Number of nodes requiresd
#SBATCH --ntasks=8 # Number of cores required
#SBATCH --partition=P4V12_SKY32M192_D # Partition/queue to run the job in.
#SBATCH -e slurm-%j.err # Error file
#SBATCH -o slurm-%j.out # Output file
#SBATCH -A gol_mtjo228_uksr
#SBATCH --mail-type ALL # Send email when job starts/ends
#SBATCH --mail-user YourBuleId@uky.edu # Where email is sent to
#SBATCH --gres=gpu:1 Specify number of using GPUs
module load ccs/singularity
singularity run MyKaldiWithnvidia.sif ./run.sh
```
Finally, the only thing we need to do is change the file to executable mod and submit the job:
```
$ chmod +x KaldiJobSub.sh
$ sbatch KaldiJobSub.sh
```