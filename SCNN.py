import tensorflow as tf
import numpy as np
from tensorflow.python.keras.models import Sequential
from tensorflow.python.keras.layers import InputLayer, Input
from tensorflow.python.keras.layers import Reshape, MaxPool2D
from tensorflow.python.keras.layers import Conv2D, Dense, Flatten, Dropout
from tensorflow.python.keras.preprocessing.image import ImageDataGenerator
from tensorflow.python.keras.models import Model
from tensorflow.python.keras.optimizers import RMSprop, Adam
from tensorflow.python.keras.applications import vgg16
from tensorflow.python.keras.utils import multi_gpu_model
from sklearn.metrics import confusion_matrix
import itertools
import h5py
from argparse import ArgumentParser
from scipy import linalg
from tensorflow.python.keras import backend as K
import scipy.ndimage
#import matplotlib.pyplot as plt
from tensorflow.keras.callbacks import TensorBoard
import time

# Argument parsing setup
parser = ArgumentParser(description='Single GPU implementation for KTC GRC project')
parser.add_argument('-e','--numepoch', metavar='', type=int, help='Number of EPOCH', default=10)
parser.add_argument('-b','--batchsize', metavar='', type=int, help='Batch size', default=200)
parser.add_argument('-g','--numgpus', metavar='', type=int, help='Number of GPUs to train', default=1)
parser.add_argument('-c','--numcpus', metavar='', type=int, help='Number of CPUs to read data', default=2)
parser.add_argument('-d','--data', metavar='', type=str, help='Data folder', default='TrainDataKeras/')
parser.add_argument('-l','--learnrate', metavar='', type=float, help='Learning rate', default=0.0001)
args = parser.parse_args()

if __name__ == '__main__':

    BATCH_SIZE = args.batchsize
    NUM_EPOCH = args.numepoch
    NUM_GPUS = args.numgpus
    NUM_CPUS = args.numcpus
    DATA_FOLDER = args.data
    LEARNING_RATE = args.learnrate

    print('\n')
    print('-- Configuration --')
    print('Batch size = ', BATCH_SIZE)
    print('Num. of EPOCH = ', NUM_EPOCH)
    print('Num. of GPUs = ', NUM_GPUS)
    print('Num. of CPUs = ', NUM_CPUS)
    print('Data folder = ', DATA_FOLDER)
    print('Learning rate = ', LEARNING_RATE)
    print('TF version = ', tf.__version__)
    print('Kearas version = ', tf.keras.__version__)
    print('\n')

    train_path = DATA_FOLDER + 'train'
    valid_path = DATA_FOLDER  + 'valid'
    test_path = DATA_FOLDER + 'test'

    #print(train_path, valid_path, test_path)
    print('-- Preparing data --')
    train_batches = ImageDataGenerator().flow_from_directory(train_path,
                                                             target_size=(224,224),
                                                             classes=['Y','N'],
                                                             batch_size=BATCH_SIZE)
    valid_batches = ImageDataGenerator().flow_from_directory(valid_path,
                                                             target_size=(224,224),
                                                             classes=['Y','N'],
                                                             batch_size=BATCH_SIZE)
    test_batches = ImageDataGenerator().flow_from_directory(test_path,
                                                            target_size=(224,224),
                                                            classes=['Y','N'],
                                                            batch_size=BATCH_SIZE)

    STEP_PER_EPOCH_TRAIN = int(train_batches.n / BATCH_SIZE)
    STEP_PER_EPOCH_VALID = int(valid_batches.n / BATCH_SIZE)
    STEP_PER_EPOCH_TEST = int(test_batches.n / BATCH_SIZE)

    print('Steps required to loop one EPOCH for training =', STEP_PER_EPOCH_TRAIN)
    print('Steps required to loop one EPOCH for validating =', STEP_PER_EPOCH_VALID)
    print('Steps required to loop one EPOCH for testing =', STEP_PER_EPOCH_TEST)
    print('\n')

    cur_time = time.strftime("%Y%m%d_%H%M%S")
    model_weight_name = cur_time + '_numofepoch(' + str(NUM_EPOCH) + ')_batchsize(' + str(BATCH_SIZE) + ').h5'
    model_name = cur_time + '_numofepoch(' + str(NUM_EPOCH) + ')_batchsize(' + str(BATCH_SIZE) + ').json'
    tensorboard = TensorBoard('log/'+cur_time+'_numofepoch(' + str(NUM_EPOCH) + ')_batchsize(' + str(BATCH_SIZE) +')')

    print('-- Create Keras Model --')
    if NUM_GPUS > 1:
        # Instantiate the base model with CPU
        with tf.device('/cpu:0'):
            model = Sequential()
            model.add(Conv2D(32, kernel_size=(3 ,3), activation='relu', padding='same', input_shape=(224, 224, 3)))
            model.add(MaxPool2D(pool_size=(2 ,2), strides=2))
            model.add(Conv2D(64, kernel_size=(3 ,3), activation='relu', padding='same'))
            model.add(MaxPool2D(pool_size=(2 ,2), strides=2))
            model.add(Conv2D(64, kernel_size=(3 , 3), activation='relu', padding='same'))
            model.add(MaxPool2D(pool_size=(2 ,2), strides=2))
            model.add(Flatten())
            model.add(Dense(128, activation='relu'))
            model.add(Dropout(0.5))
            model.add(Dense(2, activation='softmax'))
            model.summary()

        # Make parallel model with multiple GPUs
        model = multi_gpu_model(model, gpus=NUM_GPUS)
    else:
        model = Sequential()
        model.add(Conv2D(32, kernel_size=(3 ,3), activation='relu', padding='same', input_shape=(224, 224, 3)))
        model.add(MaxPool2D(pool_size=(2 ,2), strides=2))
        model.add(Conv2D(64, kernel_size=(3 ,3), activation='relu', padding='same'))
        model.add(MaxPool2D(pool_size=(2 ,2), strides=2))
        model.add(Conv2D(64, kernel_size=(3 , 3), activation='relu', padding='same'))
        model.add(MaxPool2D(pool_size=(2 ,2), strides=2))
        model.add(Flatten())
        model.add(Dense(128, activation='relu'))
        model.add(Dropout(0.5))
        model.add(Dense(2, activation='softmax'))
        model.summary()

    print('\n')

    # Start training
    print('-- Start training --')
    model.compile(Adam(lr=LEARNING_RATE), loss='categorical_crossentropy', metrics=['accuracy'])
    model.fit_generator(train_batches,
                        steps_per_epoch=STEP_PER_EPOCH_TRAIN,
                        validation_data=valid_batches,
                        validation_steps=STEP_PER_EPOCH_VALID,
                        epochs=NUM_EPOCH,
                        verbose=1,
                        callbacks=[tensorboard],
                        workers=NUM_CPUS,
                        use_multiprocessing=False)
    print('\n')

    # Save weights
    print('-- Saving model --')
    model.save_weights(model_weight_name)

    # Save architecture
    with open(model_name, 'w') as f:
        f.write(model.to_json())
    print('\n')

    # Calculate CM
    print('-- Calculate Confusion Matrix --')
    all_test_labels = []
    all_predictions = []

    # Reset
    test_batches.reset
    total_misclassifications = 0

    for i in range(STEP_PER_EPOCH_TEST):
        print('Processing [' + str(i) + ']th batch for test dataset')

        # Get next batch
        test_imgs, test_labels = next(test_batches)

        # Get labels
        test_labels = test_labels[:,0]

        # Add test labels
        all_test_labels.extend(test_labels)

        # Lets try to predict
        predictions = model.predict(test_imgs, batch_size=BATCH_SIZE, verbose=2)

        # Add all predictions
        all_predictions.extend(predictions)

        # Find misclassifications
        pred_rounded = np.round(predictions[:,0])

        for i in range(len(test_labels)):
            if test_labels[i] != pred_rounded[i]:
                total_misclassifications = total_misclassifications + 1
                print('Total number of misclassification = ', total_misclassifications)

    # Convert to ndarray
    all_test_labels = np.array(all_test_labels)
    all_predictions = np.array(all_predictions)
    all_predictions = np.round(all_predictions[:,0])

    # Calculate confusion matrix and show it
    cm = confusion_matrix(all_test_labels, all_predictions)

    print('confusion matrix=', cm)
    print('\n')